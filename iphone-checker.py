from dataclasses import dataclass
from email.mime.text import MIMEText
from typing import List
from prettytable import PrettyTable
import requests
import datetime as dt
import smtplib
from email.mime.multipart import MIMEMultipart
from typing import List
import os


URL = 'https://www.apple.com/shop/retail/pickup-message'
PICKUP_AVAILABLE = 'available'
MODEL = ['MQ003'+'LL/A', 'MQ8P3'+'LL/A'] # iphone 14 pro 128gb silver
ZIPCODE = '01803' # Burlington zipcode

@dataclass
class Iphone:
    name: str
    model: str
    address: str
    available: str

    def get_row(self):
        return [self.name, self.available, self.address]

    def __str__(self):
        return "{0}\t {1}\t {2}".format(self.name, self.available, self.address)


class IphoneStock:
    def __init__(self) -> None:
        pass
   
    def search_stores_in_zipcode(self, zipcode=ZIPCODE):
        result = []

        for i in MODEL:
            response = requests.get(URL, params={
                'pl': True,
                'location': zipcode,
                'parts.0': i
            })
           
           
            for store in response.json().get('body', {}).get('stores', []):
                message = store['partsAvailability'][i]['messageTypes']['regular']
                result.append(Iphone(name=message['storePickupProductTitle'], available=store['partsAvailability'][i]['pickupSearchQuote'], address=store['address']['address'], model=i))
           
        return result

    def print_table(self, list_of_Iphones):
        table = PrettyTable(['Model', 'Availibility', 'Store'])
        for iphone in list_of_Iphones:
            table.add_row(iphone.get_row())
        print(table)
        return table.get_string()


    def send(self, list_of_Iphones) -> None:
        now = dt.datetime.now()

        email_message = MIMEMultipart()  # enables html content
        email_message["From"] = "thami_iphone14@hotmail.com"
        email_message["To"] = "thami.amrani@hotmail.com"
        email_message["Subject"] = f"test"
        body = MIMEText(self.print_table(list_of_Iphones), 'plain')
        email_message.attach(body)
        smtp = None

        try:
            smtp = smtplib.SMTP("smtp-mail.outlook.com", 587)
            smtp.starttls()
            smtp.login("thami_iphone14@hotmail.com", os.getenv("EMAIL_PASSWORD"))
            smtp.sendmail("thami_iphone14@hotmail.com", "thami.amrani@hotmail.com", email_message.as_string())
            print("Email sent!")
        except Exception as e:  # noqa: E722
            print("Failed to send email :(")
            print(e)
        finally:
            if smtp is not None:
                smtp.quit()


if __name__ == '__main__':
    i = IphoneStock()
    r = i.search_stores_in_zipcode()
    i.print_table(r)